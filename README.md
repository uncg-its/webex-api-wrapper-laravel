# WebEx API Library - Laravel Wrapper

Contact: matt.libera@uncg.edu

# Introduction

This package is a Laravel wrapper for the UNCG WebEx API PHP Library package, so that the WebEx API PHP Library can be used in Laravel apps.

> This is a **work in progress**. Not recommend for production apps just yet.

---

# Installation

1. `composer require 'uncgits/webex-api-wrapper-laravel'`
2. *IF running Laravel 5.4 or below*: Add `Uncgits\WebexApiLaravel\ServiceProvider::class,` to your `config/app.php` file
3. Run `php artisan vendor:publish --provider='Uncgits\WebexApiLaravel\ServiceProvider'` - to publish the `webex-api.php` config file
4. Set your environment credentials in your `.env` file, and set your configuration options in `config/webex-api.php`

> Note: do not use protocols (http:// or https://) for `WEBEX_API_XML_HOST` or `WEBEX_API_NBR_HOST`

```
WEBEX_API_XML_HOST=
WEBEX_API_NBR_HOST=

WEBEX_API_USERNAME=
WEBEX_API_PASSWORD=
WEBEX_API_SITE_NAME=
WEBEX_API_PARTNER_ID=
```

## Dependencies

This package has dependencies on `uncgits/webex-api-php-library` and `barryvdh/laravel-debugbar` (dev)

---

# Usage

## Basic Usage / Getting Started

In your code, assuming you've set your information/credentials properly in your `.env` file, you should be able to instantiate the `Uncgits\WebexApiLaravel\WebexApi` class, and then use any of its available methods (inherited from `Uncgits\WebexApi\WebexApi`) to make an API call.

## Configuration options

### History Days

You can add the `WEBEX_API_HISTORY_DAYS=x` option in your `.env` file to extend the timeframe in which the historical API calls are made (e.g. to fetch session and recording data) to x days. Bear in mind that WebEx pulls at most 500 records at a time from most of its history endpoints, so if your WebEx site sees regular heavy use, it may be impractical to set this to more than 30 (default). Recordings are also not retrievable past 28 days, per Cisco's documentation.

### Notification Mode

There are two notification options built into this library. You may flash the information on the screen as a flash message, or log it in the Laravel logs (whatever you've got set).

In the `config/webex-api.php` file, `notification_mode` is an array. Inside, add one or both of `flash` and `log`.

If you choose to roll this into a higher-level custom notification / logging system for your app, you can leave this array empty to disable both of these methods, and then handle your own logging based on the return array from the API call (the API class returns the entire response, including response code, from every call).

> As of version 0.7, no flash messaging package is included. You are free to install your own - recommend `laracasts/flash` or something that implements a similar API.

### Throwing Exceptions

You can decide whether the library throws Exceptions on a failed API call, or simply returns the status and details in the response array. By default the library will throw Exceptions (`WebexApiException` class) when there is a failure in the API call, with the resulting response array being available via the exception's `getResponse()` method. You can turn this off if you do not want Exceptions to be thrown on error.

You can change this behavior by either editing the configuration file manually or adding the `WEBEX_API_THROW_EXCEPTIONS` variable to your `.env` file and setting it to a non-truthy value (false, 0, etc.).

### Debug Mode

_Requires DebugBar by Barryvdh (`barryvdh\laravel-debugbar`)_

If you turn on Debug Mode in the `config/webex-api.php` file, extra information about the call will be written to the Debugbar. This includes timing, hashed cache key, and full results from each call.

 > Note: in accordance with best practices for this package, this will only work if the app environment is not set to `production`

## Caching

For speed, adherence to rate limits, and overall performance, caching is built into this library. This utilizes the Laravel Cache mechanism (which, like Logs, can be set however you want in the Laravel config). You can optionally disable this by adding `WEBEX_API_CACHING=off` in your `.env` file.

### Cache TTL

The TTL of the cache is set to 10 minutes by default, but by adding `WEBEX_API_CACHE_MINUTES=x` to your `.env` file, you can set the cache to expire after x minutes.

### Cached endpoints

Caching is performed only for specific endpoints (e.g. requests that would equate to typical `GET` requests). Obviously, you would not want to cache requests that are designed to actually modify information in WebEx (equivalent to `POST` or `PATCH` or `DELETE` requests).

By default, there is a basic set of endpoints set up in `config/webex-api.php`. If you would like change which endpoints are / are not cached (or add your own), simply modify the `cached_endpoints` array in the config file.

---

# Version History

### 0.8.1

- change to PSR-4 declaration since we were already following it

### 0.8

- Fixes config keys for HTTP proxy

> NOTE: if upgrading, make sure you add the new config keys into your `webex-api.php` config file, and set them in your `.env` file if using a proxy.

### 0.7.1

- Change `env()` calls to `config()` calls so that config caching can be used.

### 0.7

- Removes `standaniels/flash` as a dependency. Optional to install a flash package.

### 0.6.2

- Fixes for `IsActiveWebexUser` rule: correct return array key, and handling of Exceptions.

### 0.6.1

- catch all exceptions during `xmlApiCall()` so that we don't get to the caching step when we've got an exception at all.

### 0.6

- delegating the exception to the base API library instead of the wrapper class - that way we can use `try-catch` in base API calls for situations where we expect failure (e.g. searching for a session by number without knowing what platform is being used)
- no more option to turn off exception throwing

### 0.5.2

- bugfix for missing semicolon. (hooray for impatience.)

### 0.5.1

- Support for Laravel 5.8 (cache TTL handling - now using `now()->addMinutes()` when setting Cache item TTL)

### 0.5

- add exception throwing by default, with option to turn off
- requires v 0.5 of the API Library package

### 0.4

- Nothing special

### 0.3.1

- rework hashed cache key to allow for custom prepend/append values

### 0.3

- updates cache to use `$xmlHost` (for multi-environment configs)
- updates dependency on `webex-api-php-library` to `^0.3`

### 0.2.3

- Laravel 5.5+ auto discovery

### 0.2.2

- updates dependency on `webex-api-php-library` to `^0.2`

### 0.2.1

- fixes a few other lingering effects from the months > days transition

### 0.2

- changing history months to history days

### 0.1.2

- bugfix for using proxy

### 0.1.1

- bugfix for `getPastSessions()`

### 0.1

- First real release.
- `.env` integration for WebEx credentials
- History months setting
- Debug mode
- Caching
