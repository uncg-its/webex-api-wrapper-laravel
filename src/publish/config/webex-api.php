<?php

return [

    // URLs

    'xml_host' => env('WEBEX_API_XML_HOST'),
    'nbr_host' => env('WEBEX_API_NBR_HOST'),

    // CREDENTIALS

    'username'   => env('WEBEX_API_USERNAME'),
    'password'   => env('WEBEX_API_PASSWORD'),
    'site_name'  => env('WEBEX_API_SITE_NAME'),
    'partner_id' => env('WEBEX_API_PARTNER_ID'),

    // SETTINGS

    'history_days'      => env('WEBEX_API_HISTORY_DAYS', 30),
    'notification_mode' => [], // flash, log, or empty array for none

    'debug_mode' => false, // true - extra logging in Barryvdh Debugbar (required)

    // CACHE

    'cache_active'    => env('WEBEX_API_CACHING', 'on') == 'on', // set to 'on' or 'off' in .env file
    'cache_minutes'   => env('WEBEX_API_CACHE_MINUTES', 10),
    'cache_endpoints' => [
        // site
        'site.GetSite',
        'ep.GetAPIVersion',

        // user
        'user.LstsummaryUser',
        'user.GetUser',

        // session
        'history.LstmeetingusageHistory',
        'history.LsttrainingsessionHistory',
        'history.LstsupportsessionHistory',
        'history.LsteventsessionHistory',
        'ep.LstOpenSession',

        // recording
        'ep.LstRecording',
        'ep.GetRecordingInfo'
    ],

    // PROXY
    'http_proxy' => [
        'enabled' => env('WEBEX_API_USE_PROXY', false) == 'true',
        'host'    => env('WEBEX_API_PROXY_HOST', null),
        'port'    => env('WEBEX_API_PROXY_PORT', null),
    ],

];
