<?php

namespace Uncgits\WebexApiLaravel\Facades;

use Illuminate\Support\Facades\Facade;

class WebexApi extends Facade {

    /**
     * Get the binding in the IoC container
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'WebexApi'; // the IoC binding.
    }

}