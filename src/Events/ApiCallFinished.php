<?php

namespace Uncgits\WebexApiLaravel\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class ApiCallFinished
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $callMeta;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($callMeta)
    {
        $this->callMeta = $callMeta;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('webex-api');
    }
}
