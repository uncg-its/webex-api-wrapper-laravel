<?php

namespace Uncgits\WebexApiLaravel;

use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Cache;
use Barryvdh\Debugbar\Facade as Debugbar;
use Uncgits\WebexApi\WebexApi as BaseApiModel;
use Uncgits\WebexApi\Exceptions\WebexApiException;
use Uncgits\WebexApiLaravel\Events\ApiCallStarted;
use Uncgits\WebexApiLaravel\Events\ApiCallFinished;

class WebexApi extends BaseApiModel
{
    protected $historyDays;

    protected $cacheActive;
    protected $endpointsToCache;
    protected $cacheMinutes;

    protected $debugMode;
    protected $exception;

    protected $hashedCacheKeyPrepend = '';
    protected $hashedCacheKeyAppend = '';


    public function __construct()
    {

        // set vars specific to this instance of Laravel
        $this->setXmlHost(config('webex-api.xml_host'));
        $this->setUsername(config('webex-api.username'));
        $this->setPassword(config('webex-api.password'));
        $this->setSiteName(config('webex-api.site_name'));
        $this->setPartnerId(config('webex-api.partner_id'));
        $this->setApplicationName(config('app.name'));

        if (config('webex-api.http_proxy.enabled') == 'true') {
            $this->setUseProxy(true);
            $this->setProxyHost(config('webex-api.http_proxy.host'));
            $this->setProxyPort(config('webex-api.http_proxy.port'));
        }

        // history days configuration
        $this->historyDays = config('webex-api.history_days');

        // cache configuration
        $this->cacheActive = config('webex-api.cache_active');
        $this->endpointsToCache = config('webex-api.cache_endpoints');
        $this->cacheMinutes = config('webex-api.cache_minutes');

        // debug mode (not enabled in production)
        $this->debugMode = (config('app.env') == 'production') ? false : config('webex-api.debug_mode');

        $this->throwExceptions = config('webex-api.throw_exceptions');
    }

    // extra setters for cache

    /**
     * @param string $hashedCacheKeyPrepend
     *
     */
    public function setHashedCacheKeyPrepend($hashedCacheKeyPrepend)
    {
        $this->hashedCacheKeyPrepend = $hashedCacheKeyPrepend;
    }

    /**
     * @param string $hashedCacheKeyAppend
     */
    public function setHashedCacheKeyAppend($hashedCacheKeyAppend)
    {
        $this->hashedCacheKeyAppend = $hashedCacheKeyAppend;
    }


    /**
     * xmlApiCall() - performs a call to the WebEx XML API
     *
     * @param string $service - service category (per WebEx API docs)
     * @param string $request - request endpoint (per WebEx API docs)
     * @param string $bodyContent - XML to provide inside <bodyContent> tag for the call
     * @param string $asUser - (optional) - the username with which to perform the API call, if not the administrator
     *
     * @return array
     */

    protected function xmlApiCall($service, $request, $bodyContent = '', $asUser = null)
    {
        $this->exception = null; // reset it for singleton usage

        $endpoint = $service . '.' . $request;
        $hashedCacheKey = hash(
            'sha256',
            $this->hashedCacheKeyPrepend . $service . $request . $bodyContent . $asUser . $this->hashedCacheKeyAppend
        );
        $this->debugMessage('Hashed cache key: ' . $hashedCacheKey);

        if ($this->cacheActive && Cache::has($hashedCacheKey)) {
            // in cache
            $resultArray = Cache::get($hashedCacheKey);
            $returnArray = [
                'source'  => 'cache',
                'created' => Cache::get($hashedCacheKey . '-created', 'unknown')
            ];
            $message = 'API data for ' . $endpoint . ' retrieved from cache (created: ' . $returnArray['created'] . ')';
            $messageLevel = 'success';
        } else {
            $callMeta = [
                'service'     => $service,
                'request'     => $request,
                'bodyContent' => $bodyContent,
                'asUser'      => $asUser
            ];

            // not in cache or cache inactive
            $this->debugStartMeasure('apiCall', 'Making API call to WebEx');
            event(new ApiCallStarted($callMeta));
            try {
                $resultArray = parent::xmlApiCall($service, $request, $bodyContent, $asUser);
            } catch (WebexApiException $e) {
                $this->exception = $e;
            } catch (\Exception $e) {
                $this->exception = $e;
                throw $e;
            } finally {
                event(new ApiCallFinished($callMeta));
                $this->debugStopMeasure('apiCall');

                $created = time();

                // cache if the call was successful, and we are supposed to cache it...
                if (is_null($this->exception)) {
                    $messageLevel = 'success';
                    $message = 'API call to ' . $endpoint . ' successful.';

                    if ($this->cacheActive) {
                        if (in_array($endpoint, $this->endpointsToCache)) {
                            Cache::put($hashedCacheKey, $resultArray, now()->addMinutes($this->cacheMinutes));
                            Cache::put($hashedCacheKey . '-created', $created, now()->addMinutes($this->cacheMinutes));
                            $message .= ' Result cached in API cache.';
                        } else {
                            $message .= ' Not a cacheable API call.';
                        }
                    }
                } else {
                    $message = 'API error calling ' . $endpoint . ': ' . $this->exception->getMessage() . ')';
                    $messageLevel = 'danger';
                }
                $returnArray = [
                    'source'  => 'api',
                    'created' => $created
                ];
            }
        }

        // handle notifications / logging / etc.
        $notificationMode = config('webex-api.notification_mode');

        if (in_array('flash', $notificationMode)) {
            flash($message, $messageLevel);
        }

        if (in_array('log', $notificationMode)) {
            if ($messageLevel == 'danger') {
                Log::error($message);
            } else {
                Log::info($message);
            }
        }

        if (!is_null($this->exception)) {
            throw $this->exception;
        }

        $finalArray = array_merge($returnArray, $resultArray);
        $this->debugInfo($finalArray);
        return array_merge($finalArray);
    }

    // debug helpers

    protected function debugMessage($message, $label = null)
    {
        if ($this->debugMode) {
            if (!is_null($label)) {
                Debugbar::addMessage($message, $label);
            } else {
                Debugbar::addMessage($message);
            }
        }
    }

    protected function debugInfo($object)
    {
        if ($this->debugMode) {
            Debugbar::info($object);
        }
    }

    protected function debugStartMeasure($key, $label = '')
    {
        if ($this->debugMode) {
            Debugbar::startMeasure($key, $label);
        }
    }

    protected function debugStopMeasure($key)
    {
        if ($this->debugMode) {
            Debugbar::stopMeasure($key);
        }
    }


    public function getRecordings($options = [])
    {
        if (!isset($options['historyDays'])) {
            $options['historyDays'] = 28; // webex max
        }

        if ($options['historyDays'] > 28) {
            flash(
                'Notice: The WebEx API does not allow for more than 28 days of recording history, which is below the general history threshold set by this app. Using 28 as max history days for this API call.',
                'warning'
            );
            $options['historyDays'] = 28;
        }

        return parent::getRecordings($options);
    }

    public function getPastSessions($options = [])
    {
        if (!isset($options['historyDays'])) {
            $options['historyDays'] = $this->historyDays;
        }

        return parent::getPastSessions($options);
    }
}
