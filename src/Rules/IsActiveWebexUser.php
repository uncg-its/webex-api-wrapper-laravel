<?php

namespace Uncgits\WebexApiLaravel\Rules;

use Illuminate\Contracts\Validation\Rule;
use Uncgits\WebexApiLaravel\Facades\WebexApi;

class IsActiveWebexUser implements Rule
{
    private $failedUser;
    private $reason;

    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        try {
            $getUserRequest = WebexApi::getUser($value);

            // handle if no exceptions
            if ($getUserRequest['response']['result'] != 'SUCCESS') {
                $this->failedUser = $value;
                $this->reason = 'User not found';
                return false;
            }

            if ($getUserRequest['body']['use:active'] != 'ACTIVATED') {
                $this->failedUser = $value;
                $this->reason = 'User not active';
                return false;
            }

            return true;
        } catch (WebexApiException $e) {
            $this->failedUser = $value;
            $this->reason = 'User not found';
            return false;
        } catch (\Throwable $th) {
            throw $th;
        }
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'User ' . $this->failedUser . ' is not an active WebEx user in this environment: ' . $this->reason . '.';
    }
}
