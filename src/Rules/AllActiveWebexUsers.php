<?php

namespace Uncgits\WebexApiLaravel\Rules;

use Illuminate\Contracts\Validation\Rule;
use Illuminate\Support\Facades\Validator;

class AllActiveWebexUsers implements Rule
{

    private $userString;
    private $delimiter;

    private $failureMessage;

    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct($delimiter = ',')
    {
        $this->delimiter = $delimiter;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $this->userString = $value;

        $users = explode($this->delimiter, $value);

        foreach($users as $user) {
            $validator = Validator::make(['host' => $user], [
                'host' => [new IsActiveWebexUser()]
            ]);

            if ($validator->fails()) {
                $this->failureMessage = $validator->messages()->first();
                return false;
            }
        }
        return true;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return $this->failureMessage;
    }
}
